#! /usr/bin/python
# -*- coding: utf-8 -*-

"""
   Copytight 2010 Rodolphe Quiédeville <rodolphe@quiedeville.org>


    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from Pykeol import *
import sys

"""
  This is a sample file to test the Pykeol lib.
  It reads data from the samples/data.xml file
  and print on the stdout the name and geographic position
  for each station presents in the data file.
"""

def parse_data(file):
    ''' parse data file '''
    datas = PykeolXMLFile(file)

    return datas

def do_import(file):
    ''' parse the xml file '''
    datas = parse_data(file)

    for station in datas.stations:
        print station.name, station.lon, station.lat

if __name__ == '__main__':
    do_import("samples/data.xml")
