#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
   Copytight 2010 Rodolphe Quiédeville <rodolphe@quiedeville.org>


    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import xml.sax
from time import strptime
import sys, os
import urllib


class PykeolFetch(object):
    def __init__(self, key):
        self.stations = {}
        self.api_code = None
        self.api_message = None

        error_code = {0: "Success",
                      1: "Invalid key",
                      2: "Invalid version",
                      3: "Invalid command",
                      4: "Empty key",
                      5: "Empty version",
                      6: "Empty command",
                      8: "Usage limit reached"
                      }

        # fetch the datas
        data = self.__fetch(key)

        # parse the result
        self.__parse(data)

        # convert them back to lists
        self.stations = self.stations.values()

        if (self.api_code > 0):
            print "Error %s : [%s] %s" % (self.api_code, error_code[self.api_code], self.api_message)


    def __fetch(self, key):

        version = '1.0'
        command = 'getstatione'

        url = "http://data.keolis-rennes.com/xml/?version=%s&key=%s&cmd=%s" % (version, key, command)

        return urllib.urlopen(url).read()

    def __parse(self, data):
        """Parse the given XML data"""
        xml.sax.parseString(data, PykeolFileParser(self))


class Station(object):
    def __init__(self, id=None, name=None):
        self.id = id
        self.name = name
        self.slotsavailable = -1
        self.bikesavailable = -1
        self.lon = 0
        self.lat = 0

    def __repr__(self):
        return "Station(id=%r, name=%r)" % (self.id, self.name)


class PykeolXMLFile(object):
    def __init__(self, filename):
        self.filename = filename

        self.stations = {}
        self.__parse()

    def __parse(self):
        """Parse the given XML file"""
        parser = xml.sax.make_parser()
        parser.setContentHandler(PykeolFileParser(self))
        parser.parse(self.filename)

        # convert them back to lists
        self.stations = self.stations.values()

class PykeolFileParser(xml.sax.ContentHandler):
    def __init__(self, containing_obj):
        self.containing_obj = containing_obj
        self.curr_station = None
        self.isID = 0
        self.isStationNumber = 0
        self.isStationName = 0
        self.isSlotsAvailable = 0
        self.isBikesAvailable = 0
        self.isLon = 0
        self.isLat = 0
        self.isLastUpdate = 0
        
    def startElement(self, name, attrs):
        if name == 'status':
            self.containing_obj.api_code = int(attrs['code'])
            self.containing_obj.api_message = attrs['message']
        elif name == 'station':
            self.curr_station = Station()
        elif name == 'id':
            self.isID = 1
        elif name == 'number':
            self.isStationNumber = 1
        elif name == 'name':
            self.isStationName = 1
        elif name == 'slotsavailable':
            self.isSlotsAvailable = 1
        elif name == 'bikesavailable':
            self.isBikesAvailable = 1
        elif name == 'lastupdate':
            self.isLastUpdate = 1
        elif name == 'longitude':
            self.isLon = 1
        elif name == 'latitude':
            self.isLat = 1


    def endElement(self, name):
        if name == 'station':
            self.containing_obj.stations[self.curr_station.id] = self.curr_station
            self.curr_station = None
        elif name == 'id':
            self.isID = 0
        elif name == 'number':
            self.isStationNumber = 0
        elif name == 'name':
            self.isStationName = 0
        elif name == 'slotsavailable':
            self.isSlotsAvailable = 0
        elif name == 'bikesavailable':
            self.isBikesAvailable = 0
        elif name == 'lastupdate':
            self.isLastUpdate = 0
        elif name == 'longitude':
            self.isLon = 0
        elif name == 'latitude':
            self.isLat = 0

    def characters (self, ch):
        if self.isID == 1:
            self.curr_station.id = ch
        elif self.isStationNumber == 1:
            self.curr_station.number = ch
        elif self.isStationName == 1:
            self.curr_station.name = ch
        elif self.isSlotsAvailable == 1:
            self.curr_station.slotsavailable = ch
        elif self.isBikesAvailable == 1:
            self.curr_station.bikesavailable = ch
        elif self.isLastUpdate == 1:
            self.curr_station.lastupdate = strptime(ch[0:19],'%Y-%m-%dT%H:%M:%S' )
        elif self.isLon == 1:
            self.curr_station.lon = ch
        elif self.isLat == 1:
            self.curr_station.lat = ch
